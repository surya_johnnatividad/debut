var doc = $(document);
var win = $(window);
var custom = {};

custom = {
  defaults: {
    // CONSTANT VARIABLES
  },

  onReady: function(e) {
      var vm = this;
      var el = vm.defaults;
      vm.onInit();
      vm.onResize();
      vm.productAccordion();
      vm.filterTitle();
    },  
    onInit: function() {
      var vm = this;
    },
    onLoad: function() {
      var vm = this;
      vm.testiCarousel();
      vm.bestDeals();
      vm.productsCollection();
      vm.registerAddressUnwrap();
    },
    onResize: function() {
      var vm = this;
    },
    onScroll: function() {
      var vm = this;
    },

    testiCarousel: function() {
      var $testimonial_carousel = $('.testimonial_carousel');
      $testimonial_carousel.flickity({
        cellAlign: 'center',
        draggable: true,
        prevNextButtons: false,
        lazyLoad: 1,
        autoPlay: true,
        adaptiveHeight: true
      }); 
    },     
    bestDeals: function() {
      var $collectionlist = $('.collectionlist-section .grid--uniform');
      $collectionlist.flickity({
        cellAlign: 'center',
        draggable: true,
        prevNextButtons: false,
        lazyLoad: 1,
        autoPlay: true,
        adaptiveHeight: true,
        groupCells: 2,
        watchCSS: true
      }); 
    },
    productsCollection: function() {
      var $collectionsection = $('.collectionsection ul.grid--uniform');
      $collectionsection.flickity({
        cellAlign: 'center',
        draggable: true,
        lazyLoad: 1,
        autoPlay: true,
        adaptiveHeight: true,
        groupCells: true,
        watchCSS: true
      }); 
    },
    productAccordion: function() {
      $('.faq-detail .faq__question').click(function() {
        var current_status = $(this).parent().find(".faq__answer").css('display');
        $('.faq__question').removeClass('active');
        $('.faq-detail .faq__answer').slideUp();
        //$(this).addClass('active');
        if(current_status == 'none') {
          $(this).addClass('active');
          //$(this).next().addClass('active');
          $(this).parent().find(".faq__answer").slideDown();
        }
      });      
    },
    registerAddressUnwrap: function() {
      var dTags = $(".cf-app__address-field");
      if(dTags.parent().is("div.cf-app__address-field")) {
        dTags.unwrap();
      }
    },
    filterTitle: function() {
      var newtitle = $('.filtertitle');
      newtitle.each(function(){
        var vm = $(this);
        setTimeout(function() {
          vm.insertBefore('#bc-sf-filter-options-wrapper');
        }, 3000);          
      });          
    }                    
};

doc.ready(function() {
    custom.onReady();
});

win.on('load', function() {
    custom.onLoad();
});

win.resize(function() {
    custom.onResize();
});

win.scroll(function() {
    custom.onScroll();
});

